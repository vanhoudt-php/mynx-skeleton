# Mynx

## A Drupal Distribution made at Liones

Mynx will be a Drupal Distribution that contains common contrib modules, common functionality as custom modules and common configuration as custom features. It is heavily inspired by Panopoly.
Project specific functionality can be added in additional custom modules, project specific configuration can be layered on top of the common configuration by creating a new feature with Features Overrides.

Mynx also contains a base theme with a common structure and common functionality which can be extended by modifying it directly with project specific code.

## Mynx Repositories

This repository contains all the Mynx specific modules (based of Panopoly).
The mynx-skeleton repository contains a kraftwagen skeleton based on Mynx, just like the panopoly branch of the basic skeleton maintained by kraftwagen.
The mynx-demo repository contains a demo project created with the mynx-skeleton for demo, testing and reference purpose.
