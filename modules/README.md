This directory contains all the modules. Please read the README.md files in the
subdirectory to find out the purpose of these directories. There shouldn't be
any modules directly inside this directory.
