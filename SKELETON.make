core = 7.x
api = 2

; Kraftwagen

projects[kw_environment][type] = "module"
projects[kw_environment][download][type] = "git"
projects[kw_environment][download][url] = "https://github.com/kraftwagen/kw-environment.git"
projects[kw_environment][subdir] = "kraftwagen"

projects[kw_itemnames][type] = "module"
projects[kw_itemnames][download][type] = "git"
projects[kw_itemnames][download][url] = "https://github.com/kraftwagen/kw-itemnames.git"
projects[kw_itemnames][subdir] = "kraftwagen"

projects[kw_manifests][type] = "module"
projects[kw_manifests][download][type] = "git"
projects[kw_manifests][download][url] = "https://github.com/kraftwagen/kw-manifests.git"
projects[kw_manifests][subdir] = "kraftwagen"

; Contrib - Chaos Tools & Panels

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"
projects[ctools][patch][1925018] = "http://drupal.org/files/ctools-1925018-61.patch"

projects[panels][version] = "3.x-dev"
projects[panels][subdir] = "contrib"
projects[panels][download][type] = "git"
projects[panels][download][revision] = "2bb470e"
projects[panels][download][branch] = "7.x-3.x"

projects[panels_breadcrumbs][version] = "2.1"
projects[panels_breadcrumbs][subdir] = "contrib"

projects[panelizer][version] = "3.1"
projects[panelizer][subdir] = "contrib"

projects[fieldable_panels_panes][version] = "1.5"
projects[fieldable_panels_panes][subdir] = "contrib"

projects[pm_existing_pages][version] = "1.4"
projects[pm_existing_pages][subdir] = "contrib"

projects[fape][version] = "1.x-dev"
projects[fape][subdir] = "contrib"
projects[fape][download][type] = "git"
projects[fape][download][revision] = "1143ee2"
projects[fape][download][branch] = "7.x-1.x"

; Contrib - Entities

projects[entity][version] = "1.2"
projects[entity][subdir] = "contrib"

projects[entityreference][version] = "1.0"
projects[entityreference][subdir] = "contrib"

projects[entityreference_prepopulate][version] = "1.3"
projects[entityreference_prepopulate][subdir] = "contrib"

projects[inline_entity_form][version] = "1.3"
projects[inline_entity_form][subdir] = "contrib"

projects[entitycache][version] = "1.x-dev"
projects[entitycache][subdir] = "contrib"
projects[entitycache][download][type] = "git"
projects[entitycache][download][revision] = "7e390b5"
projects[entitycache][download][branch] = "7.x-1.x"

projects[pathauto][version] = "1.x-dev"
projects[pathauto][subdir] = "contrib"
projects[pathauto][download][type] = "git"
projects[pathauto][download][revision] = "56b95e8"
projects[pathauto][download][branch] = "7.x-1.x"

; Contrib - Fields & Forms & HTML 5

projects[ds][version] = "2.6"
projects[ds][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

projects[field_group][version] = "1.3"
projects[field_group][subdir] = "contrib"

projects[link][version] = "1.1"
projects[link][subdir] = "contrib"

projects[advanced_link][version] = "1.3"
projects[advanced_link][subdir] = "contrib"

projects[simple_gmap][version] = "1.0"
projects[simple_gmap][subdir] = "contrib"

projects[field_permissions][version] = "1.0-beta2"
projects[field_permissions][subdir] = "contrib"

projects[image_url_formatter][version] = "1.4"
projects[image_url_formatter][subdir] = "contrib"

projects[rel][version] = "1.x-dev"
projects[rel][subdir] = "contrib"
projects[rel][download][type] = "git"
projects[rel][download][revision] = "c445cf6"
projects[rel][download][branch] = "7.x-1.x"

projects[elements][version] = "1.x-dev"
projects[elements][subdir] = "contrib"
projects[elements][download][type] = "git"
projects[elements][download][revision] = "7a00791"
projects[elements][download][branch] = "7.x-1.x"

projects[fences][version] = "1.x-dev"
projects[fences][subdir] = "contrib"
projects[fences][download][type] = "git"
projects[fences][download][revision] = "67206b5"
projects[fences][download][branch] = "7.x-1.x"

projects[html5_tools][version] = "1.x-dev"
projects[html5_tools][subdir] = "contrib"
projects[html5_tools][download][type] = "git"
projects[html5_tools][download][revision] = "11e0c28"
projects[html5_tools][download][branch] = "7.x-1.x"

projects[email][version] = "1.x-dev"
projects[email][subdir] = "contrib"
projects[email][download][type] = "git"
projects[email][download][revision] = "b4b5ec9"
projects[email][download][branch] = "7.x-1.x"

projects[phone][version] = "1.x-dev"
projects[phone][subdir] = "contrib"
projects[phone][download][type] = "git"
projects[phone][download][revision] = "173dd71fc755c61ed7757f3ea5d7e12ce970e5d9"
projects[phone][download][branch] = "7.x-1.x"

projects[addressfield][version] = "1.x-dev"
projects[addressfield][subdir] = "contrib"
projects[addressfield][download][type] = "git"
projects[addressfield][download][revision] = "c5cfac2"
projects[addressfield][download][branch] = "7.x-1.x"

projects[postal_code_validation][version] = "1.2"
projects[postal_code_validation][subdir] = "contrib"

projects[webform][version] = "4.x-dev"
projects[webform][subdir] = "contrib"
projects[webform][download][type] = "git"
projects[webform][download][revision] = "c9d7596"
projects[webform][download][branch] = "7.x-4.x"

projects[webform_validation][version] = "1.3"
projects[webform_validation][subdir] = "contrib"

projects[webform_phone][version] = "1.14"
projects[webform_phone][subdir] = "contrib"

projects[field_validation][version] = "2.x-dev"
projects[field_validation][subdir] = "contrib"
projects[field_validation][download][type] = "git"
projects[field_validation][download][revision] = "5e01874"
projects[field_validation][download][branch] = "7.x-2.x"

projects[fapi_validation][version] = "2.x-dev"
projects[fapi_validation][subdir] = "contrib"
projects[fapi_validation][download][type] = "git"
projects[fapi_validation][download][revision] = "a3cd4cb"
projects[fapi_validation][download][branch] = "7.x-2.x"

projects[clientside_validation][version] = "1.x-dev"
projects[clientside_validation][subdir] = "contrib"
projects[clientside_validation][download][type] = "git"
projects[clientside_validation][download][revision] = "626e2cb"
projects[clientside_validation][download][branch] = "7.x-1.x"

; Contrib - Views

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.1"
projects[views_bulk_operations][subdir] = "contrib"

projects[better_exposed_filters][version] = "3.x-dev"
projects[better_exposed_filters][subdir] = "contrib"
projects[better_exposed_filters][download][type] = "git"
projects[better_exposed_filters][download][revision] = "08f0995"
projects[better_exposed_filters][download][branch] = "7.x-3.x"

projects[views_load_more][version] = "1.x-dev"
projects[views_load_more][subdir] = "contrib"
projects[views_load_more][download][type] = "git"
projects[views_load_more][download][revision] = "28de384"
projects[views_load_more][download][branch] = "7.x-1.x"

projects[nodequeue][version] = "2.x-dev"
projects[nodequeue][subdir] = "contrib"
projects[nodequeue][download][type] = "git"
projects[nodequeue][download][revision] = "da058c0"
projects[nodequeue][download][branch] = "7.x-2.x"

projects[draggableviews][version] = "2.x-dev"
projects[draggableviews][subdir] = "contrib"
projects[draggableviews][download][type] = "git"
projects[draggableviews][download][revision] = "4f5ac4c"
projects[draggableviews][download][branch] = "7.x-2.x"

projects[views_linkarea][version] = "1.x-dev"
projects[views_linkarea][subdir] = "contrib"
projects[views_linkarea][download][type] = "git"
projects[views_linkarea][download][revision] = "ef9c00a"
projects[views_linkarea][download][branch] = "7.x-2.x"

; Contrib - Files & Media

projects[file_entity][version] = "2.0-alpha2"
projects[file_entity][subdir] = "contrib"

projects[system_stream_wrapper][version] = "1.x-dev"
projects[system_stream_wrapper][subdir] = "contrib"
projects[system_stream_wrapper][download][type] = "git"
projects[system_stream_wrapper][download][revision] = "e67a72b"
projects[system_stream_wrapper][download][branch] = "7.x-1.x"

projects[remote_stream_wrapper][version] = "1.x-dev"
projects[remote_stream_wrapper][subdir] = "contrib"
projects[remote_stream_wrapper][download][type] = "git"
projects[remote_stream_wrapper][download][revision] = "cdc7b0c"
projects[remote_stream_wrapper][download][branch] = "7.x-1.x"

projects[media][version] = "2.x-dev"
projects[media][subdir] = "contrib"
projects[media][download][type] = "git"
projects[media][download][revision] = "74ef96b"
projects[media][download][branch] = "7.x-2.x"

projects[media_youtube][version] = "2.x-dev"
projects[media_youtube][subdir] = "contrib"
projects[media_youtube][download][type] = "git"
projects[media_youtube][download][revision] = "5faa00c"
projects[media_youtube][download][branch] = "7.x-2.x"

projects[media_vimeo][version] = "2.x-dev"
projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][download][type] = "git"
projects[media_vimeo][download][revision] = "26b2eee"
projects[media_vimeo][download][branch] = "7.x-2.x"

projects[imagecache_actions][version] = "1.4"
projects[imagecache_actions][subdir] = "contrib"

; Contrib - Features

projects[features][version] = "2.0-rc3"
projects[features][subdir] = "contrib"

projects[features_override][version] = "2.0-beta3"
projects[features_override][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

; Contrib - Devel

projects[devel][version] = "1.x-dev"
projects[devel][subdir] = "contrib"
projects[devel][download][type] = "git"
projects[devel][download][revision] = "0bf83029239dfac6f75152d83f41f6acd47e1cbf"
projects[devel][download][branch] = "7.x-1.x"

projects[simplehtmldom][version] = "1.12"
projects[simplehtmldom][subdir] = "contrib"

projects[devel_themer][version] = "1.x-dev"
projects[devel_themer][subdir] = "contrib"
projects[devel_themer][download][type] = "git"
projects[devel_themer][download][revision] = "78b616a"
projects[devel_themer][download][branch] = "7.x-1.x"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

; Contrib - UX/UI Improvements

projects[backports][version] = "1.0-alpha1"
projects[backports][subdir] = "contrib"

projects[module_filter][version] = "1.8"
projects[module_filter][subdir] = "contrib"

projects[simplified_menu_admin][version] = "1.0-beta2"
projects[simplified_menu_admin][subdir] = "contrib"

projects[date_popup_authored][version] = "1.1"
projects[date_popup_authored][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[admin_views][version] = "1.2"
projects[admin_views][subdir] = "contrib"

projects[save_draft][version] = "1.4"
projects[save_draft][subdir] = "contrib"

projects[auto_entitylabel][version] = "1.x-dev"
projects[auto_entitylabel][subdir] = "contrib"
projects[auto_entitylabel][download][type] = "git"
projects[auto_entitylabel][download][revision] = "fabb255"
projects[auto_entitylabel][download][branch] = "7.x-1.x"

projects[publishcontent][version] = "1.2"
projects[publishcontent][subdir] = "contrib"

projects[scheduler][version] = "1.x-dev"
projects[scheduler][subdir] = "contrib"
projects[scheduler][download][type] = "git"
projects[scheduler][download][revision] = "a2e9d66"
projects[scheduler][download][branch] = "7.x-1.x"

; Contrib - Search

projects[search_api][version] = "1.x-dev"
projects[search_api][subdir] = "contrib"
projects[search_api][download][type] = "git"
projects[search_api][download][revision] = "c89fd1affbdc22f46722b5396fe680e5b8ac10ae"
projects[search_api][download][branch] = "7.x-1.x"

projects[facetapi][version] = "1.x-dev"
projects[facetapi][subdir] = "contrib"
projects[facetapi][download][type] = "git"
projects[facetapi][download][revision] = "77069510977ff502d2d5e10153fff98c96f21c3e"
projects[facetapi][download][branch] = "7.x-1.x"

; Contrib - WYSIWYG

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][patch][1489096] = "http://drupal.org/files/wysiwyg-table-format.patch"
projects[wysiwyg][patch][1786732] = "http://drupal.org/files/wysiwyg-arbitrary_image_paths_markitup-1786732-3.patch"
projects[wysiwyg][patch][1802394] = "http://drupal.org/files/wysiwyg-1802394-4.patch"

projects[wysiwyg_filter][version] = "1.6-rc2"
projects[wysiwyg_filter][subdir] = "contrib"

projects[image_resize_filter][version] = "1.13"
projects[image_resize_filter][subdir] = "contrib"

projects[caption_filter][version] = "1.2"
projects[caption_filter][subdir] = "contrib"

projects[transliteration][version] = "3.x-dev"
projects[transliteration][subdir] = "contrib"
projects[transliteration][download][type] = "git"
projects[transliteration][download][revision] = "a4aba37"
projects[transliteration][download][branch] = "7.x-3.x"

projects[linkit][version] = "2.6"
projects[linkit][subdir] = "contrib"

projects[pathologic][version] = "2.11"
projects[pathologic][subdir] = "contrib"

; Contrib - Cache

projects[apc][version] = "1.x-dev"
projects[apc][subdir] = "contrib"
projects[apc][download][type] = "git"
projects[apc][download][revision] = "9b0b6f6a564a0e8ad3a885b1700abb581d7b7edd"
projects[apc][download][branch] = "7.x-1.x"
projects[apc][patch][cli-no-requirements-check] = "https://bitbucket.org/liones-php/drupal-patches/raw/da1ed4070b6a26322cf56a85f79d01f31a0f1683/apc/apc-cli-no-requirements-check.patch"

projects[memcache][version] = "1.x-dev"
projects[memcache][subdir] = "contrib"
projects[memcache][download][type] = "git"
projects[memcache][download][revision] = "cf758f792cd5ad1efdfc0f722741ff01f1dda7fd"
projects[memcache][download][branch] = "7.x-1.x"

projects[varnish][version] = "1.x-dev"
projects[varnish][subdir] = "contrib"
projects[varnish][download][type] = "git"
projects[varnish][download][revision] = "e6726b4a3ae4d1ab2adeb0b0038e06d2995921cd"
projects[varnish][download][branch] = "7.x-1.x"

projects[expire][version] = "2.x-dev"
projects[expire][subdir] = "contrib"
projects[expire][download][type] = "git"
projects[expire][download][revision] = "d1e93563f14d7103099eaf35e2209a63b0f12300"
projects[expire][download][branch] = "7.x-2.x"

projects[purge][version] = "2.x-dev"
projects[purge][subdir] = "contrib"
projects[purge][download][type] = "git"
projects[purge][download][revision] = "dc5b22d91487082d5ffe7a75935a1296af73d822"
projects[purge][download][branch] = "7.x-2.x"

; Contrib - Misc

projects[jquery_update][version] = "2.3"
projects[jquery_update][subdir] = "contrib"

projects[google_analytics][version] = "1.3"
projects[google_analytics][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[rules][version] = "2.5"
projects[rules][subdir] = "contrib"

projects[advanced_help][version] = "1.0"
projects[advanced_help][subdir] = "contrib"

projects[libraries][version] = "2.1"
projects[libraries][subdir] = "contrib"

projects[variable][version] = "2.3"
projects[variable][subdir] = "contrib"

projects[metatag][version] = "1.x-dev"
projects[metatag][subdir] = "contrib"
projects[metatag][download][type] = "git"
projects[metatag][download][revision] = "2fdc262"
projects[metatag][download][branch] = "7.x-1.x"

projects[xmlsitemap][version] = "2.x-dev"
projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][download][type] = "git"
projects[xmlsitemap][download][revision] = "bbce7d6"
projects[xmlsitemap][download][branch] = "7.x-2.x"

; Mynx

projects[mynx_core][type] = "module"
projects[mynx_core][download][type] = "git"
projects[mynx_core][download][url] = "https://bitbucket.org/liones-php/mynx-core.git"
projects[mynx_core][subdir] = "mynx"

projects[mynx_cache][type] = "module"
projects[mynx_cache][download][type] = "git"
projects[mynx_cache][download][url] = "https://bitbucket.org/liones-php/mynx-cache.git"
projects[mynx_cache][subdir] = "mynx"

projects[mynx_admin][type] = "module"
projects[mynx_admin][download][type] = "git"
projects[mynx_admin][download][url] = "https://bitbucket.org/liones-php/mynx-admin.git"
projects[mynx_admin][subdir] = "mynx"

projects[mynx_breadcrumb][type] = "module"
projects[mynx_breadcrumb][download][type] = "git"
projects[mynx_breadcrumb][download][url] = "https://bitbucket.org/liones-php/mynx-breadcrumb.git"
projects[mynx_breadcrumb][subdir] = "mynx"

projects[mynx_landing_pages][type] = "module"
projects[mynx_landing_pages][download][type] = "git"
projects[mynx_landing_pages][download][url] = "https://bitbucket.org/liones-php/mynx-landing-pages.git"
projects[mynx_landing_pages][subdir] = "mynx"

projects[mynx_media][type] = "module"
projects[mynx_media][download][type] = "git"
projects[mynx_media][download][url] = "https://bitbucket.org/liones-php/mynx-media.git"
projects[mynx_media][subdir] = "mynx"

projects[mynx_menus][type] = "module"
projects[mynx_menus][download][type] = "git"
projects[mynx_menus][download][url] = "https://bitbucket.org/liones-php/mynx-menus.git"
projects[mynx_menus][subdir] = "mynx"

projects[mynx_overview_pages][type] = "module"
projects[mynx_overview_pages][download][type] = "git"
projects[mynx_overview_pages][download][url] = "https://bitbucket.org/liones-php/mynx-overview-pages.git"
projects[mynx_overview_pages][subdir] = "mynx"

projects[mynx_panels][type] = "module"
projects[mynx_panels][download][type] = "git"
projects[mynx_panels][download][url] = "https://bitbucket.org/liones-php/mynx-panels.git"
projects[mynx_panels][subdir] = "mynx"

projects[mynx_search][type] = "module"
projects[mynx_search][download][type] = "git"
projects[mynx_search][download][url] = "https://bitbucket.org/liones-php/mynx-search.git"
projects[mynx_search][subdir] = "mynx"

projects[mynx_taxonomy][type] = "module"
projects[mynx_taxonomy][download][type] = "git"
projects[mynx_taxonomy][download][url] = "https://bitbucket.org/liones-php/mynx-taxonomy.git"
projects[mynx_taxonomy][subdir] = "mynx"

projects[mynx_text_pages][type] = "module"
projects[mynx_text_pages][download][type] = "git"
projects[mynx_text_pages][download][url] = "https://bitbucket.org/liones-php/mynx-text-pages.git"
projects[mynx_text_pages][subdir] = "contrib"

projects[mynx_theme][type] = "module"
projects[mynx_theme][download][type] = "git"
projects[mynx_theme][download][url] = "https://bitbucket.org/liones-php/mynx-theme.git"
projects[mynx_theme][subdir] = "mynx"

projects[mynx_users][type] = "module"
projects[mynx_users][download][type] = "git"
projects[mynx_users][download][url] = "https://bitbucket.org/liones-php/mynx-users.git"
projects[mynx_users][subdir] = "mynx"

projects[mynx_widgets][type] = "module"
projects[mynx_widgets][download][type] = "git"
projects[mynx_widgets][download][url] = "https://bitbucket.org/liones-php/mynx-widgets.git"
projects[mynx_widgets][subdir] = "mynx"

projects[mynx_wysiwyg][type] = "module"
projects[mynx_wysiwyg][download][type] = "git"
projects[mynx_wysiwyg][download][url] = "https://bitbucket.org/liones-php/mynx-wysiwyg.git"
projects[mynx_wysiwyg][subdir] = "mynx"

; Libraries

libraries[backbone][download][type] = "get"
libraries[backbone][download][url] = "https://github.com/documentcloud/backbone/archive/1.0.0.zip"

libraries[underscore][download][type] = "get"
libraries[underscore][download][url] = "https://github.com/documentcloud/underscore/archive/1.4.4.zip"

libraries[SolrPhpClient][download][type] = "get"
libraries[SolrPhpClient][download][url] = "http://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.zip"
; libraries[solrphpclient][download][type] = get
; libraries[solrphpclient][download][url] = http://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.zip

; Get specially patched version of these libraries with Drupal specific fixes,
; courtesy of Panopoly and Pantheon.
libraries[tinymce][download][type] = get
libraries[tinymce][download][url] = http://apps.getpantheon.com/sites/all/libraries/tinymce-panopoly.tar.gz

libraries[markitup][download][type] = get
libraries[markitup][download][url] = http://apps.getpantheon.com/sites/all/libraries/markitup-panopoly.tar.gz

libraries[jquery.cycle][download][type] = get
libraries[jquery.cycle][download][url] = http://apps.getpantheon.com/sites/all/libraries/jquery-cycle.tar.gz

libraries[respondjs][download][type] = get
libraries[respondjs][download][url] = http://apps.getpantheon.com/sites/all/libraries/panopoly-respondjs.tar.gz
