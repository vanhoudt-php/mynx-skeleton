name = ***HUMAN_NAME***
description = Installs a Mynx based site for ***HUMAN_NAME*** with Kraftwagen.
core = 7.x

; The dependencies below determine which modules are enabled when the site has
; finished installing.

; Core
dependencies[] = block
dependencies[] = color
dependencies[] = comment
dependencies[] = contextual
dependencies[] = dblog
dependencies[] = field
dependencies[] = field_sql_storage
dependencies[] = field_ui
dependencies[] = file
dependencies[] = filter
dependencies[] = help
dependencies[] = image
dependencies[] = list
dependencies[] = locale
dependencies[] = menu
dependencies[] = node
dependencies[] = number
dependencies[] = options
dependencies[] = path
dependencies[] = shortcut
dependencies[] = syslog
dependencies[] = system
dependencies[] = taxonomy
dependencies[] = text
dependencies[] = translation
dependencies[] = user

; Kraftwagen
dependencies[] = kw_environment
dependencies[] = kw_itemnames
dependencies[] = kw_itemnames_entity
dependencies[] = kw_itemnames_file
dependencies[] = kw_itemnames_menu_link
dependencies[] = kw_itemnames_url_alias
dependencies[] = kw_itemnames_user_role
dependencies[] = kw_manifests

; Contrib - Chaos Tools & Panels

dependencies[] = ctools
dependencies[] = page_manager
dependencies[] = panels
dependencies[] = panels_ipe
dependencies[] = panels_breadcrumbs
dependencies[] = panelizer
dependencies[] = fieldable_panels_panes
dependencies[] = pm_existing_pages
dependencies[] = fape
dependencies[] = views_content

; Contrib - Entities

dependencies[] = entity
dependencies[] = entityreference
dependencies[] = entityreference_prepopulate
dependencies[] = inline_entity_form
dependencies[] = entitycache
dependencies[] = pathauto

; Contrib - Fields & Forms & HTML 5

dependencies[] = ds
dependencies[] = ds_extras
dependencies[] = ds_forms
dependencies[] = ds_search
dependencies[] = ds_ui
dependencies[] = date
dependencies[] = date_api
dependencies[] = date_all_day
dependencies[] = date_repeat
dependencies[] = date_repeat_field
dependencies[] = date_popup
dependencies[] = date_tools
dependencies[] = date_views
dependencies[] = entityreference
dependencies[] = field_group
dependencies[] = link
dependencies[] = advanced_link
dependencies[] = simple_gmap
dependencies[] = field_permissions
dependencies[] = image_url_formatter
dependencies[] = rel
dependencies[] = elements
dependencies[] = fences
dependencies[] = html5_tools
dependencies[] = email
dependencies[] = phone
dependencies[] = addressfield
dependencies[] = postal_code_validation
dependencies[] = webform
dependencies[] = webform_validation
dependencies[] = webform_phone
dependencies[] = field_validation
dependencies[] = field_validation_ui
dependencies[] = field_validation_extras
dependencies[] = property_validation
dependencies[] = fapi_validation
dependencies[] = clientside_validation
dependencies[] = clientside_validation_fapi
dependencies[] = clientside_validation_field_validation
dependencies[] = clientside_validation_form
dependencies[] = clientside_validation_html5
dependencies[] = clientside_validation_states
dependencies[] = clientside_validation_webform

; Contrib - Views

dependencies[] = views
dependencies[] = views_ui
dependencies[] = views_bulk_operations
dependencies[] = actions_permissions
dependencies[] = better_exposed_filters
dependencies[] = views_load_more
dependencies[] = nodequeue
dependencies[] = smartqueue
dependencies[] = draggableviews
dependencies[] = views_linkarea

; Contrib - Files & Media

dependencies[] = file_entity
dependencies[] = system_stream_wrapper
dependencies[] = remote_stream_wrapper
dependencies[] = media
dependencies[] = media_internet
dependencies[] = media_youtube
dependencies[] = media_vimeo
dependencies[] = media_wysiwyg_view_mode
dependencies[] = imagecache_actions
dependencies[] = imagecache_coloractions
dependencies[] = imagecache_customactions
dependencies[] = image_styles_admin

; Contrib - Features

dependencies[] = features
dependencies[] = features_override
dependencies[] = strongarm

; Contrib - Devel

; devel_themer (and it's dependency, simplehtmldom) should never be enabled
; automatically, and NEVER on production, only enable these modules when needed.
;dependencies[] = devel_themer

dependencies[] = diff

; Contrib - UX/UI Improvements

dependencies[] = backports
dependencies[] = module_filter
dependencies[] = simplified_menu_admin
dependencies[] = date_popup_authored
dependencies[] = admin_menu
dependencies[] = admin_menu_toolbar
dependencies[] = admin_views
dependencies[] = save_draft
dependencies[] = auto_entitylabel
dependencies[] = publishcontent
dependencies[] = scheduler

; Contrib - Search

;dependencies[] = facetapi
dependencies[] = search_api
dependencies[] = search_api_views
;dependencies[] = search_api_facetapi
dependencies[] = ds_search

; Contrib - WYSIWYG

dependencies[] = wysiwyg
dependencies[] = wysiwyg_filter
dependencies[] = image_resize_filter
dependencies[] = caption_filter
dependencies[] = transliteration
dependencies[] = linkit
dependencies[] = pathologic

; Contrib - Varnish

dependencies[] = memcache
dependencies[] = apc
dependencies[] = varnish
dependencies[] = expire
dependencies[] = purge

; Contrib - Misc

dependencies[] = jquery_update
dependencies[] = token
dependencies[] = rules
dependencies[] = rules_admin
dependencies[] = rules_scheduler
dependencies[] = advanced_help
dependencies[] = libraries
dependencies[] = variable
dependencies[] = variable_admin
dependencies[] = variable_views
dependencies[] = metatag
dependencies[] = metatag_ui
dependencies[] = metatag_opengraph
dependencies[] = metatag_panels
dependencies[] = metatag_twitter_cards
dependencies[] = metatag_views
dependencies[] = xmlsitemap
dependencies[] = xmlsitemap_custom
dependencies[] = xmlsitemap_engines
dependencies[] = xmlsitemap_menu
dependencies[] = xmlsitemap_modal
dependencies[] = xmlsitemap_node
dependencies[] = xmlsitemap_taxonomy

; Mynx
dependencies[] = mynx_core
dependencies[] = mynx_cache
dependencies[] = mynx_admin
;dependencies[] = mynx_breadcrumb
;dependencies[] = mynx_landing_pages
dependencies[] = mynx_media
;dependencies[] = mynx_menus
;dependencies[] = mynx_overview_pages
dependencies[] = mynx_panels
dependencies[] = mynx_search
;dependencies[] = mynx_taxonomy
;dependencies[] = mynx_text_pages
;dependencies[] = mynx_theme
dependencies[] = mynx_users
;dependencies[] = mynx_widgets
;dependencies[] = mynx_wysiwyg

; Development only
; env_dependencies[development][] = update
env_dependencies[development][] = devel
env_dependencies[development][] = devel_generate
env_dependencies[development][] = nodequeue_generate
env_dependencies[development][] = ds_devel

; Production only
env_dependencies[production][] = googleanalytics

files[] = ***MACHINE_NAME***.profile
