<?php

require DRUPAL_ROOT . '/includes/cache.inc';
// APC isn't enabled (or relevant) when using PHP in CLI mode,
// so don't use it as a cache backend when Drupal is in CLI mode (eg. during site install).
if (!drupal_is_cli()) {
  require DRUPAL_ROOT . '/profiles/%PROJECT_MACHINE_NAME%/modules/contrib/apc/drupal_apc_cache.inc';
}
require DRUPAL_ROOT . '/profiles/%PROJECT_MACHINE_NAME%/modules/contrib/memcache/memcache.inc';
require DRUPAL_ROOT . '/profiles/%PROJECT_MACHINE_NAME%/modules/contrib/varnish/varnish.cache.inc';

include dirname(__FILE__) . '/settings.local.php';

$update_free_access = FALSE;

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

$conf['file_public_path'] = 'sites/default/files';
$conf['file_private_path'] = '/private_files';
$conf['file_temporary_path'] = '/tmp';

// We have a reverse proxy ladies and gentlemen.
$conf['reverse_proxy'] = TRUE;
// Make sure the actual client ip is passed along to apache.
$conf['reverse_proxy_header'] = 'HTTP_X_CLUSTER_CLIENT_IP';
// Indicate that the reverse proxy is hosted on the same machine (localhost ip).
$conf['reverse_proxy_addresses'] = array('127.0.0.1');

// Page cache will be handled by Varnish, so that's possible without a database.
$conf['page_cache_without_database'] = TRUE;
// Drupal 7 does not cache pages when we invoke hooks during bootstrap, this needs to be disabled.
$conf['page_cache_invoke_hooks'] = FALSE;

if (!drupal_is_cli()) {
  // APC.
  $conf['cache_backends'][] = 'profiles/%PROJECT_MACHINE_NAME%/modules/contrib/apc/drupal_apc_cache.inc';
}

// Memcache.
$conf['cache_backends'][] = 'profiles/%PROJECT_MACHINE_NAME%/modules/contrib/memcache/memcache.inc';

// Varnish.
$conf['cache_backends'][] = 'profiles/%PROJECT_MACHINE_NAME%/modules/contrib/varnish/varnish.cache.inc';

// Memcache - Default cache for everything but base/bootstrap/page/form, see below.
$conf['cache_default_class'] = 'MemCacheDrupal';

if (!drupal_is_cli()) {
  // !cli - APC - Base cache.
  $conf['cache_class_cache'] = 'DrupalAPCCache';
  // !cli - APC - Bootstrap cache.
  $conf['cache_class_cache_bootstrap'] = 'DrupalAPCCache';
}

// Varnish - Page cache.
$conf['cache_class_cache_page'] = 'VarnishCache';

// Database - Form cache - has to be in the database,
// otherwise forms might become invalid after cache clears due to the CSRF protection mechanisms.
$conf['cache_class_form'] = 'DrupalDatabaseCache';


$conf['site_name'] = '***HUMAN_NAME***';
